#
# Cookbook Name:: dev
# Recipe:: default
#
# Copyright 2015, PPOMPPU Co., Inc.
#
# All rights reserved - Do Not Redistribute
#


# PHP
# discover the horde channel
php_pear_channel "pear.horde.org" do
  action :discover
end

# download xml then add the symfony channel
remote_file "#{Chef::Config[:file_cache_path]}/symfony-channel.xml" do
  source "http://pear.symfony-project.com/channel.xml"
  mode 0644
end

php_pear_channel "symfony" do
  channel_xml "#{Chef::Config[:file_cache_path]}/symfony-channel.xml"
  action :add
end

# update the main pear channel
php_pear_channel 'pear.php.net' do
  action :update
end

# update the main pecl channel
php_pear_channel 'pecl.php.net' do
  action :update
end

case node[:platform]
when "ubuntu","debian"
  %w{php5-memcached php5-gd php5-mcrypt php5-curl php5-gearman php5-redis php5-memcache php5-xdebug}.each do |pkg|
    package pkg do
      action :upgrade
    end
  end

  %w{php5-json php5-mysql}.each do |pkg|
    package pkg do
      action :upgrade
    end
  end
when "centos"
  # TODO: sorry.
  package "libmemcached" do
    action :install
  end

  package "libmemcached-dev" do
    action :install
  end

  package "php-pecl-memcache.x86_64" do
    centos :install
  end
end

# PHP-FPM
include_recipe "php-fpm"
php_fpm_pool "www" do
  process_manager "dynamic"
  max_requests 5000
  php_options 'php_admin_flag[log_errors]' => 'on', 'php_admin_value[memory_limit]' => '32M'
end

# Elasticsearch
service "elasticsearch" do
  action :start
end


# PHP Composer
include_recipe "composer"
composer_project "/vagrant" do
    dev false
    quiet true
    prefer_dist false
    action :install
end
composer_project "/vagrant" do
    dev false
    quiet true
    action :update
end

# Nginx
template "#{node.nginx.dir}/sites-available/default" do
  source "nginx-default-site.erb"
  mode "0644"
end
